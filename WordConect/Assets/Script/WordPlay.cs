using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WordPlay : MonoBehaviour
{
    public static WordPlay instance;
    public List<QuestData> questList;
    public List<QuestData> check;
    public List<char> wordToChoose;
    public List<char> distinctWordToChoose;
    [SerializeField]
    private WordData[] answerArr;
    [SerializeField]
    private WordData[] wordToChooseArr;
    [SerializeField]
    private List<KeyData> keyDatas;
    public List<char> ansBar;
    private char[] charArr;
    public List<string> keyToCheck;
    private int currentAnswerIndex = 0;
    private List<int> selectedWordIndex;
    private bool isCorrectAns;
    public List<string> correctedAns;
    public GameManager gamemanager;
    private void Awake()
    {
        instance = this;
    }
    public void Start()
    {
        selectedWordIndex = new List<int>();
        SetKeyWord();
        SetWordToChoose();
        SetQuestion();
    }
    private void Update()
    {
        if(countKeyCorrected == 3)
        {
            Debug.Log("is End game");
            gamemanager.WinLevel();
        }
    }
    int maxKeyCount = 0;
    void SetKeyWord()
    {
        for (int i = 0; i < questList.Count; i++)
        {
            var keyWord = questList[i].answer;

            var list = keyDatas[i];

            for (int j = keyWord.Length; j < list.keyList.Length; j++)
            {
                list.keyList[j].gameObject.SetActive(false);
            }
            for (int k = 0; k < keyWord.Length; k++)
            {
                list.keyList[k].SetWord(keyWord[k]);

                list.keyList[k].HideWord();

            }
            keyToCheck.Add(keyWord);
            foreach (var item in keyToCheck)
            {
                item.Trim();   
            }
            for (int j = 0; j < keyToCheck.Count; j++)
            {
                if (keyToCheck[j].Length > maxKeyCount)
                {
                    maxKeyCount = keyToCheck[j].Length;
                }

            }

        }

    }
    void ResetAnsBar()
    {
        ansBar.Clear();
    }
    int countKeyCorrected = 0;
    void checkAnsAndKey()
    {
        for (int i = 0; i < questList.Count; i++)
        {         
            var keyWord = questList[i].answer;
         
            var list = keyDatas[i];          
            if (keyWord.Length == ansBar.Count)
            {
                if (!correctedAns.Contains(keyWord))
                {
                    isCorrectAns = true;
                    countKeyCorrected++;
                    for (int k = 0; k < keyWord.Length; k++)
                    {
                        if (char.ToUpper(keyWord[k]) != char.ToUpper(ansBar[k]))
                        {

                            countKeyCorrected--;
                            isCorrectAns = false;
                            break;
                        }
                    }
                    if (isCorrectAns)
                    {
                        for (int k = 0; k < keyWord.Length; k++)
                        {
                            list.keyList[k].ShowWord();
                        }
                        if (countKeyCorrected <= keyToCheck.Count)
                        {
                            Invoke("SetQuestion", 0.5f);
                            ResetAnsBar();
                        }
                        correctedAns.Add(keyWord);

                    }
                }
                

            }         
        }
        Debug.Log(countKeyCorrected);
    }
    void SetQuestion()
    {
        currentAnswerIndex = 0;   
        ResetQuest();
        selectedWordIndex.Clear();
        for (int i = 0; i < distinctWordToChoose.Count; i++)
        {
            charArr[i] = char.ToUpper(distinctWordToChoose[i]);
        }

        charArr = ShufferList.ShuffleListItems<char>(charArr.ToList()).ToArray();
        for (int i = 0; i < charArr.Length; i++)
        {
            wordToChooseArr[i].SetWord(charArr[i]);
        }
    }
    public void SelectedSet(WordData wordData)
    {

        if (currentAnswerIndex >= maxKeyCount)
            return;
        selectedWordIndex.Add(wordData.transform.GetSiblingIndex());
        wordData.gameObject.SetActive(false);
        answerArr[currentAnswerIndex].SetWord(wordData.charValue);
        var wordChoosed = char.ToUpper(answerArr[currentAnswerIndex].charValue);
        ansBar.Add(wordChoosed);
        currentAnswerIndex++;
        checkAnsAndKey();   
    }
    void SetWordToChoose()
    {
        for (int i = 0; i < keyToCheck.Count; i++)
        {
            var keyWord = keyToCheck[i];

            for (int j = 0; j < keyWord.Length; j++)
            {
                var word = char.ToUpper(keyWord[j]);
                wordToChoose.Add(word);
            }
        }
        distinctWordToChoose = wordToChoose.Distinct().ToList();
        charArr = new char[distinctWordToChoose.Count];
    }

    void ResetQuest()
    {
        for (int i = 0; i < answerArr.Length; i++)
        {
            answerArr[i].gameObject.SetActive(true);
            answerArr[i].SetWord('_');
        }
        for (int i = 0; i < wordToChooseArr.Length; i++)
        {
            wordToChooseArr[i].gameObject.SetActive(true);
        }
        currentAnswerIndex = 0;
    }
    public void ResetLastWord()
    {
        if (selectedWordIndex.Count > 0)
        {
            int index = selectedWordIndex[selectedWordIndex.Count - 1];
            wordToChooseArr[index].gameObject.SetActive(true);
            selectedWordIndex.RemoveAt(selectedWordIndex.Count - 1);

            currentAnswerIndex--;
            answerArr[currentAnswerIndex].SetWord('_');
            ansBar.RemoveAt(currentAnswerIndex);
        }
    }
    private void Reset()
    {
        SetWordToChoose();
        SetQuestion();
    }
}

