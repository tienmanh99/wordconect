using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordData : MonoBehaviour
{
    [SerializeField]
    private Text wordTxt;
    [HideInInspector]
    public char charValue;

    private Button wordObj;
    private void Awake()
    {
        wordObj = GetComponent<Button>();
        if (wordObj)
        {
            wordObj.onClick.AddListener(()=> WordSelected());
        }
    }
    public void SetWord(char value)
    {
        wordTxt.text = value + "";
        charValue = value;
    }
    public void HideWord()
    {
        wordTxt.enabled = false;
        
    }
    public void ShowWord()
    {
        wordTxt.enabled = true;
       
    }
    void WordSelected()
    {
        WordPlay.instance.SelectedSet(this);
    }

}
